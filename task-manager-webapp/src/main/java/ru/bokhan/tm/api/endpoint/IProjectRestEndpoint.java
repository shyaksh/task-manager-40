package ru.bokhan.tm.api.endpoint;

import feign.Feign;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.MediaType;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.bokhan.tm.dto.ProjectDto;

@FeignClient("project")
@RequestMapping("/api/project")
public interface IProjectRestEndpoint {

    @NotNull
    static IProjectRestEndpoint client(@NotNull final String baseUrl) {
        @NotNull final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        @NotNull final HttpMessageConverters converters = new HttpMessageConverters(converter);
        @NotNull final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(IProjectRestEndpoint.class, baseUrl);
    }

    @RequestMapping(method = {RequestMethod.POST,}, produces = MediaType.APPLICATION_JSON_VALUE)
    ProjectDto save(@NotNull final ProjectDto s);

    @GetMapping(value = "/${id}", produces = MediaType.APPLICATION_JSON_VALUE)
    ProjectDto findById(@NotNull @PathVariable("id") final String id);

    @GetMapping(value = "/exists/${id}", produces = MediaType.APPLICATION_JSON_VALUE)
    boolean existsById(@NotNull @PathVariable("id") final String id);

    @DeleteMapping(value = "/${id}", produces = MediaType.APPLICATION_JSON_VALUE)
    void deleteById(@NotNull @PathVariable("id") final String id);

}

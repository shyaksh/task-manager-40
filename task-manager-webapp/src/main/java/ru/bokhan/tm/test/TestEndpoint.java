package ru.bokhan.tm.test;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test/test")
public class TestEndpoint {

    @GetMapping("/tr")
    public String test() {
        return "aasd";
    }

}

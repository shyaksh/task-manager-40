package ru.bokhan.tm.repository.entity;

import org.jetbrains.annotations.NotNull;
import ru.bokhan.tm.entity.Task;

public interface TaskRepository extends AbstractRepository<Task> {

    void deleteByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    void deleteByUserIdAndName(@NotNull final String userId, @NotNull final String name);

    void deleteByUserId(@NotNull final String userId);

}
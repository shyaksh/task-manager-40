package ru.bokhan.tm.repository.dto;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.bokhan.tm.dto.AbstractEntityDTO;

public interface AbstractRepositoryDTO<E extends AbstractEntityDTO> extends JpaRepository<E, String> {

}

package ru.bokhan.tm.constant;

import org.jetbrains.annotations.NotNull;

public interface ReflectionConstant {

    @NotNull
    String COMMAND_PACKAGE_PATH = "ru.bokhan.tm.command";

}
